# Biocomputacion - Alineamiento de Reads. Descripcion de partes claves del proyecto.

> **Integrantes:** Alice Franco, Miryam Godoy, Daniel Min.

## Como ejecutar el proyecto.

Desde la terminal de linux

`$ python3 Proyecto_01_Alineamiento.py`

o

`$ python Proyecto_01_Alineamiento.py`

Se recomienda la primera forma.


## **Seccion #1:** Sobre como tomar los nucleotidos de la vecindad de la particion coincidente.

Suponga que tenemos un read de la forma `GAGTTTTATCGCTTCCATGACGCAGAAGTTAACACTTTCGGATATTTC` y particionamos en 4 partes
(porque k era 3) `[GAGTTTTATCGCTTCC]`, `[ATGACGCAGAAGTTAA]` y `[CACTTTCGGATATTTC]`.

En vez de tomar los nucleotidos del genoma original en forma simetrica con respecto a la particion coincidente, se toma tantos
nucleotidos del lado derecho e izquierdo como nucleotidos queden en las correspondientes particiones restantes. Ej.:
si la primera particion resulta coincidente, se toman todos los nucleotidos del genoma original hacia la derecha de
la particion coincidente.

## **Seccion #2:** Sobre como generar matriz de alineamiento.

Una vez que se tiene la lista de reads en orden relativo al genoma original con solapamientos,
se rellena el lado izquierdo de cada read con los nucleotidos que se encuentran solamente en el read
predecesor y no en el actual. Ej, sea un read `ATCGGC` y uno siguiente `CGGCAA`. Entonces el lado izquierdo
del segundo read se rellena con `AT` quedando `AT` + `CGGCAA` = `ATCGGCAA` y asi sucesivamente para todos los reads.

## **Seccion #3:** Sobre como generar matriz de alineamiento.

Se aplica la misma logica de la seccion #2 para rellenar el lado derecho de cada reads ordenados segun su
posicion relativa al genoma original.

## **Seccion #4:** Sobre como obtener el consenso.

Todos los reads que ademas de estar ordenados, ahora tienen la misma longitud. Luego cada read es descompuesto en
nucleotidos individuales tales que cada nucleotido de cada read ocupe una columna individual.

Se tiene una matriz de caracteres compuestos de los nucleotidos. Se traspone la matriz y luego se vuelven
a unir los caracteres de cada fila de la traspuesta. Con esto se obtiene por cada fila, una cadena de nucleotidos
que corresponden a los nucleotidos que se solapaban en cada posicion, en el alineamiento (antes de la trasposicion).

Al final de esto, se recorre secuencialmente cada fila de la traspuesta (que corresponde a una posicion en el alineamiento)
y se le da salida al nucleotido que tiene mayor frecuencia.
