#! /usr/bin/python3

import bisect
import collections
import time

import numpy as np


class Index(object):
    """
    Class to handle index structure.

    Extract from Coursera.
    """

    def __init__(self, t, k):
        self.k = k
        self.index = []
        for i in range(len(t) - k + 1):
            self.index.append((t[i:i + k], i))
        self.index.sort()

    def query(self, p):
        kmer = p[:self.k]
        i = bisect.bisect_left(self.index, (kmer, -1))
        hits = []
        while i < len(self.index):
            if self.index[i][0] != kmer:
                break
            hits.append(self.index[i][1])
            i += 1
        return hits


def query_index(p, t, index):
    """
    Perform query to the index and return a list of offset where `p` match `t`.

    Extract from Coursera.

    :param p: The pattern (or the read)
    :param t: The text (or the genome)
    :param index: The index objecto to query
    :return: List of offsets at where `p` match `t`.
    """

    k = index.k
    offsets = []
    for i in index.query(p):  # 'hits' is empty if the index doesn't contain the left-most-k-mer of p in the index.
        if p[k:] == t[i + k:i + len(p)]:  # verification of the right-most remaining characters of p against t.
            offsets.append(i)
    return offsets


def read_genome(file_name):
    """
    Obtain the whole genome cointained in the FASTA file.

    Extract from Coursera.

    :param file_name: The absolute path to the file.
    :return: The whole genome.
    """
    genome = ''
    with open(file_name, 'r') as f:
        for line in f:
            if not line[0] == '>':
                genome += line.rstrip()  # rstrip() remove any kind onf spaces or new line at the end of the string.
    return genome


def read_fastq(file_name):
    """
    Obtain the reads contains in a FASTQ file.

    Extract from Coursera.

    :param file_name: The abs path to the FASTQ file.
    :return: The list of reads.
    """
    sequences = []
    qualities = []
    with open(file_name) as fh:
        while True:
            fh.readline()  # tag line
            sequence = fh.readline().rstrip()
            fh.readline()  # + sign
            quality = fh.readline().rstrip()
            if len(sequence) == 0:
                break
            sequences.append(sequence)
            qualities.append(quality)
    return sequences, qualities


def edit_distance(v, w, pieces, desde, k=2):
    """Calculate the edit distance matrix and then,
    build a list of reads in the order in which
    they could appear in the original genome.

    Parameters
    ----------
    v, w : str
        Strings representing `v` to the pattern P and `w` the text T.
    pieces : list
        Hold the read `p` and his relative position from the genome,
        iff the edit distance is less than or equal than k.
    desde : integer
        Indicates the position at witch the pattern should occur.
    k : integer 
    """
    p = v  # p is the read
    t = w  # t is the genome

    edit_distance_matrix = np.zeros((len(v) + 1, len(w) + 1))
    arrow_path_matrix = np.zeros((len(v) + 1, len(w) + 1))

    for i in range(len(v) + 1):
        for j in range(len(w) + 1):
            if i == 0:
                edit_distance_matrix[i][j] = 0
            else:
                if j == 0:
                    edit_distance_matrix[i][j] = i
                else:
                    set_value(edit_distance_matrix, arrow_path_matrix, v, w, i, j)

    i = len(edit_distance_matrix) - 1  # from last row of edit distance matrix
    least = min(edit_distance_matrix[i])  # take the minimum
    col_of_least = np.argmin(edit_distance_matrix[i])  # hold the col and row at witch the previous value appear.
    row_of_least = i

    # Allow only reads with less than or equal to `k` edit distance.
    if least <= k:
        start = find_start_pos(row_of_least, col_of_least, arrow_path_matrix)
        pieces.append((desde + start, p))


def find_start_pos(row, col, arrow_path_matrix):
    """
    Figure out where the matching is start, from the edit distance matrix.
    
    :param row: The last row of the matrix.
    :param col: The column in witch is the smallest value. 
    :param arrow_path_matrix: The directional arrow matrix for edit distance matrix.
    :return: The col in witch the matching is start.
    """
    while True:
        if row == 0 or col == 0:
            return col

        if arrow_path_matrix[row][col] == 1200:
            row -= 1
        else:
            if arrow_path_matrix[row][col] == 1030:
                col -= 1
                row -= 1
            else:
                col -= 1


def set_value(edit_distance_matrix, arrow_path_matrix, v, w, i, j):
    """Calculate and put values to the edit distance matrix."""

    lowest = len(v) + len(w)

    if v[i - 1] == w[j - 1]:
        value = edit_distance_matrix[i - 1][j - 1]
        if value < lowest:
            lowest = value
            edit_distance_matrix[i][j] = lowest
            arrow_path_matrix[i][j] = '1030'
            return

    if v[i - 1] != w[j - 1]:
        # looking out North-West-side
        value = edit_distance_matrix[i - 1][j - 1] + 1
        if value < lowest:
            lowest = value
            edit_distance_matrix[i][j] = lowest
            arrow_path_matrix[i][j] = '1030'

        # looking out West-side
        value = edit_distance_matrix[i][j - 1] + 1
        if value < lowest:
            lowest = value
            edit_distance_matrix[i][j] = lowest
            arrow_path_matrix[i][j] = '0900'

        # looking out North-side
        value = edit_distance_matrix[i - 1][j] + 1
        if value < lowest:
            lowest = value
            edit_distance_matrix[i][j] = lowest
            arrow_path_matrix[i][j] = '1200'


def transpose(A, B):
    for i in range(len(A)):
        for j in range(len(A[0])):
            B[i][j] = A[j][i]


def main():
    genome = read_genome('phix.fa')
    secs, quals = read_fastq('reads_phix_1.fastq')
    t = genome
    k = 2
    index = Index(t, len(secs[0]) // (k + 1))
    pieces = []

    time_c = 0
    time0 = time.time()
    for p in secs:
        read_len, chunk_size = len(p), len(p) // (k + 1)
        partitions = [p[i:i + chunk_size] for i in range(0, read_len, chunk_size)]
        partition_nro = -1
        partition_size = len(partitions)
        hit = []
        for i in range(len(partitions)):
            partition = partitions[i]
            if len(partition) < chunk_size: continue  # skip partition smaller than the chunk_size

            hit = query_index(partition, t, index)
            if not (len(hit) == 0):
                partition_nro = i
                break  # if one of the partitions match any section of T, take the P and construct edit distance matrix.

        if partition_nro == -1:
            continue

        # Improving the how to take all surrounding nucleotides of the genome, around the matched partition.
        # See Readme.md section #1 for more information.
        desde = hit[0] - partition_nro * chunk_size
        desde = desde if desde > -1 else 0
        hasta = hit[0] + (partition_size - partition_nro) * chunk_size + (len(p) - chunk_size * (len(partitions)))
        tt = t[desde:hasta]

        time_a = time.time()

        # Calculate Edit distance and also construct the list of the reads
        # in the order they should appear in the genome.
        edit_distance(tt, p, pieces, desde)

        time_b = time.time()
        time_c += (time_b - time_a)
    time_d = time.time()
    time_e = (time_d - time0) - time_c

    # Sort the reads by his relative position to the genome.
    pieces.sort()

    # Fill left-side of subsequent reads with the previous read nucleotides.
    # See Readme.md section #2 for more information.
    aligned_reads = []
    prev_read = ''
    for startAt, read in pieces:
        if prev_read == '':
            item = read
        else:
            item = prev_read[0:startAt] + read

        aligned_reads.append(item)
        prev_read = item

        # if startAt > 57:
        #     break

    # Fill the right-side of all fist-most reads with the next read nucleotides.
    # See Readme.md section #3 for more information.
    i = len(aligned_reads) - 1
    prev_read = aligned_reads[i]
    i -= 1
    while i >= 0:
        aligned_reads[i] = aligned_reads[i] + (prev_read[-(len(prev_read) - len(aligned_reads[i])):])
        prev_read = aligned_reads[i]
        i -= 1

    # After finished the left-padding proccess,
    # we should take the last row of the reads
    # and then we get he sequenced genome, but
    # we wanted to implement the consensus.

    # sequenced_raw = aligned_reads[len(aligned_reads) - 1]

    # Implementing consensus. See Readme.md section #4 for more information.
    unit_chars_array = [list(aligned_reads[i]) for i in range(len(aligned_reads))]
    trasposed = [[row[i] for row in unit_chars_array] for i in range(max(len(r) for r in unit_chars_array))]
    list_of_nucleotides = [''.join(trasposed[i]) for i in range(len(trasposed))]

    sequenced_raw = ''
    for nucleotides in list_of_nucleotides:
        count = collections.Counter(nucleotides)
        for nucleotide, _ in count.most_common(1):
            sequenced_raw = sequenced_raw + nucleotide
            break

    # Stroke sequenced genome in rows of 70 nucleotides, to apply FASTA format.
    n = 70
    file_name = "SequencedGenome.fa"
    format_fasta = '\n'.join([sequenced_raw[i:i + n] for i in range(0, len(sequenced_raw), n)])

    # Saving to file
    with open(file_name, 'w') as file:
        file.write('>gi||||Genoma secuenciado.\n')
        file.write(format_fasta)

    time_f = time.time()

    # Time measurement.
    print("===========================================")
    print('File generated:', file_name)
    print("===========================================")
    print("Preprocessing time:", time_e)
    print('Ordered-read listing time:', time_c)
    print('Whole program:', (time_f - time0))
    print("===========================================")

    # Credits
    # Remove first element from tuple : https://stackoverflow.com/questions/32908954/python-removing-first-element-of-tuple-in-a-list/32909007
    # Join tuple's element into a list : https://www.geeksforgeeks.org/python-join-tuple-elements-in-a-list/
    # Return the index of minimum element : https://docs.scipy.org/doc/numpy/reference/generated/numpy.argmin.html
    # Insert a symbol every nth character into a string : https://forums.freebsd.org/threads/python-inserting-a-sign-after-every-n-th-character.26881/


if __name__ == "__main__": main()
